<?php
	// Настройки подключения к базе данных
	$config['db']['host']	 = 'localhost';
	$config['db']['charset'] = 'utf8';
	$config['db']['user']	 = '';
	$config['db']['pwd']	 = '';
	$config['db']['db']		 = '';
	
	// Путь к директории админки, например "/admin/"
	$config['system']['admin_dir'] = 'admin';
	
	// Основной URL-адрес сайта
	$config['site']['domain'] = '';
	
	// Настройки доступа к почте (SMTP)
	$config['mail']['host']	= 'smtp.yandex.ru';
	$config['mail']['port']	= 465;
	$config['mail']['user']	= '';
	$config['mail']['pass']	= '';
	$config['mail']['ebox']	= '';
	$config['mail']['name']	= '';
	
	// Настройки изображений для фотогалереи
	$config['photogallery']['formats'] = array(
		array('folder' => 'original'),										// оригинал
		// array('folder' => 'vb',	'w' => 1600, 'h' => 850, 'q' => 80),	// очень большое изображение
		array('folder' => 'b',	'w' =>  780, 'h' => 438, 'q' => 80),		// большое изображение
		array('folder' => 'm',	'w' =>  220, 'h' => 165, 'q' => 90),		// среднее изображение
		array('folder' => 's',	'w' =>  100, 'h' =>  56, 'q' => 90),		// маленькое изображение
		// array('folder' => 'l',	'w' =>   83, 'h' =>  46, 'q' => 90)		// очень маленькое изображение
	);
	
	$config['photogallery']['adm_preview'] = 's';
	$config['photogallery']['adm_bigprev'] = 'm';
	$config['photogallery']['adm_picture'] = 'b';
	
	// Максимальное кол-во фото в фотогалерее
	$config['photogallery']['limit'] = 4;
?>