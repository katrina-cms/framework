<?php
	// Настройки путей
	define('DS',				DIRECTORY_SEPARATOR);
	define('INDEX',				dirname(__FILE__).DS);
	define('MODULES',			INDEX.'modules'.DS);
	define('CORE',				INDEX.'core'.DS);
	define('PLUGINS',			CORE.'plugins'.DS);
	define('APPLICATION',		INDEX.'app'.DS);
	define('BACKUPS',			INDEX.'backups'.DS);
	define('CONFIG',			INDEX.'config'.DS);
	define('CACHE',				INDEX.'cache'.DS);
	
	// Прочие настройки
	define('HOST',				$_SERVER['HTTP_HOST']);
	define('HTTP_HOST',			'http://'.HOST.'/');
	define('URI_BASE',			'/');
	define('OFFICE_IP',			'213.178.34.98');
	define('OFFICE',			(bool)($_SERVER['REMOTE_ADDR'] == OFFICE_IP));
	define('OFFICE_IL_IP',		'217.113.123.36');
	define('OFFICE_IL',			(bool)($_SERVER['REMOTE_ADDR'] == OFFICE_IL_IP));
	define('KDV_IP',			'62.106.119.123');
	define('KDV',				(bool)($_SERVER['REMOTE_ADDR'] == KDV_IP));
	define('VERSION',			'1.01');
	define('DEBUG',				true);
	
	define('ALERT_CHANGE_DATA',	'Данные были успешно изменены');
	define('ALERT_DEL_IMAGE',	'Изображение было успешно удалено');
	
	// Устанавливаем часовой пояс
	date_default_timezone_set('Europe/Moscow');
	
	// Устанавливаем показ ошибок
	if (OFFICE === true || OFFICE_IL === true || KDV === true) {
		error_reporting(E_ALL);
		ini_set('display_errors', 'On');
	}
	
	// Загружаем автозагрузчик классов
	require_once CORE.'core_autoload.php';
	
	// Запускаем приложение
	// if (OFFICE === true || OFFICE_IL === true || KDV === true) {
		new core_application();
	// } else {
		// echo('...');
		// die();
	// }
?>