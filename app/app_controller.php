<?php
	class app_controller extends core_controller {
		
		static protected $user_id		= 0;			// id если мы аутентифицированы
		static protected $title			= array();		// содержимое тега <title>
		static protected $asides		= array();		// массив боковых блоков
		static protected $volume_id		= 0;			// "main.id" активного раздела
		static protected $volume_path	= null;		    // массив активных путей
		static protected $layout		= 'default';	// название корневого шаблона
		
		public function __before() {
			parent::__before();
			
			// инициализация заголовка браузера
			self::$title[] = htmlspecialchars_decode($this->_config->get('title_browser', 'site'));
			
			// подключаем общие JS файлы
            $this->_js->assign('//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js', true);
		}
		
		public function __after() {
			parent::__after();
						
			// обработка сообщений типа "alert"
			if ($this->_tpl->assign('alert')) {
				$this->_tpl->assign('alert', '<script type="text/javascript">alert("'.$this->_tpl->assign('alert').'");</script>');
			}

			if ($alert = $this->_session->get('alert')) {
				$this->_tpl->assign('alert', '<script type="text/javascript">alert("'.$alert.'");</script>');
				$this->_session->del('alert');
			}
			
			// добавляем заголовок в шаблонизатор
			$this->_tpl->assign('site_title',		join(' :: ', array_reverse(self::$title)));
						
			// генерируем файлы ресурсов
			$this->_tpl->assign('assigned_less',    $this->_less->generate());
			$this->_tpl->assign('assigned_css',		$this->_css->generate());
			$this->_tpl->assign('assigned_js',		$this->_js->generate());
			
			// выводим всё на экран
			echo $this->_tpl->render('layouts/'.self::$layout);
			die();
		}
		
		/**
		 * Страница 404
		 * 
		 * Должна обязательно выводится с заголовком HTTP 404
		 */
		protected function page_404() {
			self::$title[] = '404 Страница не найдена';
			header('HTTP/1.0 404 Not Found');
			$this->_tpl->render('layouts/_page_404', false, 'content');
		}
	}
?>